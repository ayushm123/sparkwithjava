package com.jobreadyprogrammer.spark;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import static org.apache.spark.sql.functions.*;

public class Application {

    public static void main(String[] args) {

        SparkSession spark = SparkSession.builder()
                .appName("Learning Spark SQL Dataframe API")
                .master("local")
                .getOrCreate();


        String studentsFile = "src/main/resources/students.csv";

        Dataset<Row> studentDf = spark.read().format("csv")
                .option("inferSchema", "true") // Make sure to use string version of true
                .option("header", true)
                .load(studentsFile);

        String gradeChartFile = "src/main/resources/grade_chart.csv";

        Dataset<Row> gradesDf = spark.read().format("csv")
                .option("inferSchema", "true") // Make sure to use string version of true
                .option("header", true)
                .load(gradeChartFile);

//        studentDf.join(gradesDf, studentDf.col("gpa").equalTo(gradesDf.col("gpa")))
//                .where(studentDf.col("gpa").between(2, 3.5))
//                .select(studentDf.col("student_name")
//                ,studentDf.col("favorite_book_title"),gradesDf.col("letter_grade")).show();

        Dataset<Row> filteredData = studentDf.join(gradesDf, studentDf.col("gpa").equalTo(gradesDf.col("gpa")))
                .where(studentDf.col("gpa").gt(2).and(studentDf.col("gpa").lt(3.5)).or(studentDf.col("gpa").equalTo(1.0)))
                .select(studentDf.col("student_name")
                        , studentDf.col("favorite_book_title")
                        , gradesDf.col("letter_grade"));

        filteredData.show();


    }


}
