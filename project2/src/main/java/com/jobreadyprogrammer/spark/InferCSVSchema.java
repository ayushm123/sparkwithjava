package com.jobreadyprogrammer.spark;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

public class InferCSVSchema {

    public void printSchema() {
        SparkSession session = new SparkSession.Builder()
                .appName("Complex CSV to DataFrame")
                .master("local")
                .getOrCreate();

        Dataset<Row> df = session.read().format("csv")
                .option("sep", ";")
                .option("header", true)
                .option("multiline", true)
                .option("quote", "^")
                .option("dateFormar", "M/d/y")
                .option("inferSchema", true)
                .load("src/main/resources/amazonProducts.txt");


        df.show(7,90);

        System.out.println("Schema for the csv file");

        df.printSchema();


    }

}
