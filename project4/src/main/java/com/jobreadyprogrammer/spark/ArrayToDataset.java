package com.jobreadyprogrammer.spark;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.api.java.function.ReduceFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

public class ArrayToDataset {

	public void start() {
		SparkSession spark = new SparkSession.Builder()
				.appName("Array To Dataset<String>")
				.master("local")
				.getOrCreate();
		
		String [] stringList = new String[] {"Banana", "Car", "Glass", "Banana", "Computer", "Car"};
		
		List<String> data = Arrays.asList(stringList);
		
		Dataset<String> ds = spark.createDataset(data,Encoders.STRING());

		ds = ds.map(val -> "word:"+val,Encoders.STRING());

		ds.show(10);

		String stringValue = ds.reduce(new StringReducer());

		System.out.println("The reduce value is "+stringValue);
		
	}
	
	 static class StringReducer implements ReduceFunction<String>,Serializable
	{

		@Override
		public String call(String s, String t1) throws Exception {
			return s+"-"+t1;
		}
	}

}
