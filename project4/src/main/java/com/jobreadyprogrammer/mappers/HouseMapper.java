package com.jobreadyprogrammer.mappers;

import java.text.SimpleDateFormat;

import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Row;

import com.jobreadyprogrammer.pojos.House;

public class HouseMapper implements MapFunction<Row, House>{
	@Override
	public House call(Row row) throws Exception {
		House house = new House();
		house.setId(row.getAs("id"));
		house.setAddress(row.getAs("address"));

		SimpleDateFormat parser = new SimpleDateFormat("yyyy-mm-dd");

		house.setVacantBy(parser.parse(row.getAs("vacantBy").toString()));

		house.setPrice(row.getAs("price"));

		house.setSqft(row.getAs("sqft"));

		return house;
	}

	/**
	 * 
	 */

		

	
}